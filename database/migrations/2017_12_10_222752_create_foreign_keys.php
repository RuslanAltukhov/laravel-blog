<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
                Schema::table('posts', function (Blueprint $table) {
					//$table->index('author_id');
					$table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');
				});
				  Schema::table('comments', function (Blueprint $table) {
						//$table->index('post_id');
						$table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
