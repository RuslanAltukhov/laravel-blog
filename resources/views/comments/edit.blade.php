@extends('layouts.app')
@section('title', 'Редактирование комментария')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')
<h1>Редактировать: {{ $model->id }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($model, array('route' => array('comments.update', $model->id), 'method' => 'PUT')) }}

    <div class="form-group">
      
        {{  Form::hidden('post_id',null) }}
    </div>

    <div class="form-group">
      {{ Form::label('comment', 'Комментарий') }}
        {{ Form::textarea('comment', null, ['class' => 'form-control']) }}
    </div>

    

    {{ Form::submit('Редактировать',['class' => 'btn btn-primary']) }}

{{ Form::close() }}
@endsection
