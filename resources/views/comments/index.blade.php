@extends('layouts.app')
@section('title', 'Комметарии')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')


<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Пост</td>
            <td>Комментарий</td>
           <td>Действия</td>
        </tr>
    </thead>
    <tbody>
    @foreach($models as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->post->post }}</td>
            <td>{{ $value->comment }}</td>
           

           
            <td>
                <a class="btn btn-small btn-success" href="{{ URL::to('comments/' . $value->id) }}">Показать</a> 
                <a class="btn btn-small btn-info" href="{{ URL::to('comments/' . $value->id . '/edit') }}">Редактировать</a>
				  {{ Form::open(['url' => 'comments/' . $value->id,'style'=>'display:inline-block']) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Удалить', ['class' => 'btn-small btn btn-danger']) }}
                {{ Form::close() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

