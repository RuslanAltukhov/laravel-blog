
@extends('layouts.app')
@section('title', 'Комментарии')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')
<!-- if there are creation errors, they will show here -->

{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'comments')) }}

    <div class="form-group">
      
       {{ Form::hidden('post_id', $post_id) }}
    </div>

    <div class="form-group">
        {{ Form::label('comment', 'Коментарий') }}
        {{ Form::textarea('comment', Input::old('comment'), ['class' => 'form-control']) }}
    </div>


    {{ Form::submit('Создать комментарий',['class' => 'btn btn-primary']) }}

{{ Form::close() }}

@endsection