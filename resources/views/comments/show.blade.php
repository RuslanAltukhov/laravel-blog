
@extends('layouts.app')
@section('title', 'Комментарии')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')
<h1>Просмотр {{ $model->id }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $model->post->post }}</h2>
        <p>
            <strong>Текст записи:</strong> {{ $model->comment }}<br>
        </p>
    </div>
@endsection
