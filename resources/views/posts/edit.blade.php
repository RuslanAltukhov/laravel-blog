@extends('layouts.app')
@section('title', 'Редактирование поста')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')
<h1>Редактировать: {{ $model->id }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($model, array('route' => array('posts.update', $model->id), 'method' => 'PUT')) }}

    <div class="form-group">
       {{ Form::label('author_id', 'Автор') }}
        {{  Form::select('author_id', $authors,null,['class' => 'form-control']) }}
    </div>

    <div class="form-group">
      {{ Form::label('post', 'Пост') }}
        {{ Form::textarea('post', null, ['class' => 'form-control']) }}
    </div>

    

    {{ Form::submit('Редактировать', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@endsection
