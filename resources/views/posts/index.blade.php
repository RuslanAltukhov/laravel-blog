@extends('layouts.app')
@section('title', 'Посты')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')


<a href="/posts/create" class="btn btn-success" >Создать пост</a>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Пост</td>
            <td>Автор</td>
           <td>Действия</td>
        </tr>
    </thead>
    <tbody>
    @foreach($models as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->post }}</td>
            <td>{{ $value->author->name }} ({{ $value->author->email }})</td>
           

           
            <td>
                <a class="btn btn-small btn-success" href="{{ URL::to('posts/' . $value->id) }}">Показать</a> 
				 <a class="btn btn-small btn-default" href="{{ URL::to('comments/' . $value->id . '/add-comment') }}">Написать комментарий</a> 
                <a class="btn btn-small btn-info" href="{{ URL::to('posts/' . $value->id . '/edit') }}">Редактировать</a>
				  {{ Form::open(['url' => 'posts/' . $value->id,'style'=>'display:inline-block']) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Удалить', ['class' => 'btn-small btn btn-danger']) }}
                {{ Form::close() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

