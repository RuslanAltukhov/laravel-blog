
@extends('layouts.app')
@section('title', 'Посты')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')
<!-- if there are creation errors, they will show here -->

{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'posts')) }}

    <div class="form-group">
        {{ Form::label('author_id', 'Автор') }}
        {{  Form::select('author_id', $authors,null,['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('post', 'Пост') }}
        {{ Form::textarea('post', Input::old('post'), ['class' => 'form-control']) }}
    </div>


    {{ Form::submit('Создать пост',['class' => 'btn btn-primary']) }}

{{ Form::close() }}

@endsection