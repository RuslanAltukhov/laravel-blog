
@extends('layouts.app')
@section('title', 'Посты')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')
<h1>Просмотр {{ $model->id }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $model->author->name }}</h2>
        <p>
            <strong>Текст записи:</strong> {{ $model->post }}<br>
        </p>
    </div>
@endsection
