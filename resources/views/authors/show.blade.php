@extends('layouts.app')
@section('title', 'Редактирование поста')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')

<h1>Просмотр {{ $author->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $author->name }}</h2>
        <p>
            <strong>Email:</strong> {{ $author->email }}<br>
        </p>
    </div>
@endsection
