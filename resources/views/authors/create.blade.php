@extends('layouts.app')
@section('title', 'Авторы')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')

<h1>Добавить автора</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'authors')) }}

    <div class="form-group">
        {{ Form::label('name', 'Имя') }}
        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}
    </div>


    {{ Form::submit('Добавить автора', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@endsection
