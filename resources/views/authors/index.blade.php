@extends('layouts.app')
@section('title', 'Авторы')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')
<a href="/authors/create" class="btn btn-success" >Создать автора</a>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Имя</td>
            <td>Email</td>
           <td>Действия</td>
        </tr>
    </thead>
    <tbody>
    @foreach($authors as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
           

            <!-- we will also add show, edit, and delete buttons -->
            <td>

              
                <a class="btn btn-small btn-success" href="{{ URL::to('authors/' . $value->id) }}">Показать</a>

               
                <a class="btn btn-small btn-info" href="{{ URL::to('authors/' . $value->id . '/edit') }}">Редактировать</a>
	  {{ Form::open(['url' => 'authors/' . $value->id,'style'=>'display:inline-block']) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Удалить', ['class' => 'btn-small btn btn-danger']) }}
                {{ Form::close() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

