
@extends('layouts.app')
@section('title', 'Редактирование поста')

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@section('content')



<h1>Редактировать: {{ $author->name }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($author, array('route' => array('authors.update', $author->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name', 'Имя') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', null, array('class' => 'form-control')) }}
    </div>

    

    {{ Form::submit('Редактировать', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@endsection
