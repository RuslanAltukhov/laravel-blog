<html>
    <head>
        <title>App Name - @yield('title')</title>
		 <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    </head>
    <body>
	 <div style="padding:20px" class="container">
	  <div class="row" >
	   <div class="col-md-2" >
        @section('sidebar')
          <ul class="nav-pills nav-stacked nav">
		   <li><a href="/authors" >Авторы</a></li>
		    <li><a href="/posts" >Посты</a></li>
			 <li><a href="/comments" >Комментарии</a></li>
		  </ul>
        @show
		</div>
<div class="col-md-10" >

       
            @yield('content')
			</div>
			</div>
        </div>
    </body>
</html>