<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
   protected $table = 'posts';
   public $timestamps = false;
   public function author()
   {
	 return $this->hasOne('App\Author','id','author_id');   
   }
}
