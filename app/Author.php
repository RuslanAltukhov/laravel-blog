<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
		public $timestamps = false;
      protected $table = 'authors';
	  public static function formatForInputs($models)
   {
	   $items = [];
	   foreach($models as $key => $value)
		   $items[$value->id] = $value->name." ({$value->id})";
	   return $items;
   }
	    public function posts()
    {
        return $this->hasMany('App\Post','author_id', 'id');
    }
}
