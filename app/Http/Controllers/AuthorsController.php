<?php

namespace App\Http\Controllers;
use App\Author;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Session;
use Laracasts\Flash\Flash;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
class AuthorsController extends Controller
{
    public function index()
	{
		$authors = Author::all();
		return view('authors.index', ['authors' => $authors]);
	}
	
	public function create()
	{
		return view('authors.create');
	}
	
	public function store()
	{
		 // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email',
        );
        $validator = Validator(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('authors/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $author = new Author;
            $author->name       = Input::get('name');
            $author->email      = Input::get('email');  
            $author->save();

            // redirect
           // Flash::success('Автор успешно добавлен');
            return Redirect::to('authors');
        }
		
	}
	
	public function show($id)
	{
		  // get the nerd
        $author = Author::find($id);

        // show the view and pass the nerd to it
        return View('authors.show',['author'=>$author]);
            
	}
	public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email',
      
        );
        $validator = Validator(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('authors/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $nerd = Author::find($id);
            $nerd->name       = Input::get('name');
            $nerd->email      = Input::get('email');
          
            $nerd->save();

            // redirect
           // Session::flash('message', 'Successfully updated nerd!');
            return Redirect::to('authors');
        }
    }
	 public function destroy($id)
    {
      
        $model = Author::find($id);
        $model->delete();

        return Redirect::to('posts');
    }
	 public function edit($id)
    {
        // get the nerd
        $author = Author::find($id);

        // show the edit form and pass the nerd
        return View('authors.edit',['author'=>$author]);
    }
}
