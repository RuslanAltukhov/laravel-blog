<?php

namespace App\Http\Controllers;

use App\Post;
use App\Author;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Session;
use Laracasts\Flash\Flash;
use App\Rules\AuthorExist;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class PostsController extends Controller
{
      public function index()
	{
		$models = Post::all();
		return view('posts.index', ['models' => $models]);
	}
	
	public function create()
	{
		$authors = Author::all();
		return view('posts.create',['authors'=>Author::formatForInputs($authors)]);
	}
	
	public function store()
	{
		
        $rules = array(
            'author_id'       => ['required',new AuthorExist],
            'post'      => 'required',
        );
        $validator = Validator(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('posts/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $post = new Post;
            $post->post       = Input::get('post');
            $post->author_id      = Input::get('author_id');  
            $post->save();
            return Redirect::to('posts');
        }
		
	}
	
	public function show($id)
	{
		  // get the nerd
        $model = Post::find($id);

        // show the view and pass the nerd to it
        return View('posts.show',['model'=>$model]);
            
	}
	public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'post'       => ['required',new AuthorExist],
            'author_id'      => 'required',
      
        );
        $validator = Validator(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('posts/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $post = Post::find($id);
            $post->author_id       = Input::get('author_id');
            $post->post      = Input::get('post');
            $post->save();

            // redirect
           // Session::flash('message', 'Successfully updated nerd!');
            return Redirect::to('posts');
        }
    }
	  public function destroy($id)
    {
      
        $model = Post::find($id);
        $model->delete();

        // redirect
       // Session::flash('message', 'Successfully deleted the nerd!');
        return Redirect::to('posts');
    }
	 public function edit($id)
    {
        // get the nerd
        $model = Post::find($id);
		$authors = Author::all();
        // show the edit form and pass the nerd
        return View('posts.edit',['model'=>$model,'authors'=>Author::formatForInputs($authors)]);
    }
}
