<?php

namespace App\Http\Controllers;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Session;
use Laracasts\Flash\Flash;
use App\Rules\PostExist;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CommentsController extends Controller
{
     public function index()
	{
		$models = Comment::all();
		return view('comments.index', ['models' => $models]);
	}
	
	public function addComment($id)
	{
	
		$model = new Comment;
		return view('comments.create',['post_id'=>$id,'model'=>$model]);
	}
	
	public function store()
	{
		 // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'post_id'       => ['required', new PostExist],
            'comment'      => 'required',
        );
        $validator = Validator(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('comments/'.Input::get('post_id').'/add-comment')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $comment = new Comment;
           $comment->comment       = Input::get('comment');
           $comment->post_id      = Input::get('post_id');  
            $comment->save();

         
            return Redirect::to('comments');
        }
		
	}
	
	public function show($id)
	{
		  // get the nerd
        $model = Comment::find($id);

        // show the view and pass the nerd to it
        return View('comments.show',['model'=>$model]);
            
	}
	public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'post_id'       => ['required', new PostExist],
            'comment'      => 'required',
      
        );
        $validator = Validator(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('comments/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $model = Comment::find($id);
            $model->post_id       = Input::get('post_id');
            $model->comment      = Input::get('comment');
            $model->save();

            // redirect
           // Session::flash('message', 'Successfully updated nerd!');
            return Redirect::to('comments');
        }
    }
	 public function destroy($id)
    {
      
        $model = Comment::find($id);
        $model->delete();
        return Redirect::to('comments');
    }
	 public function edit($id)
    {
        // get the nerd
        $model = Comment::find($id);

        // show the edit form and pass the nerd
        return View('comments.edit',['model'=>$model]);
    }
}
