<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	public $timestamps = false;
     
      protected $table = 'comments';
	  public function post()
   {
	 return $this->hasOne('App\Post','id','post_id');   
   }
}
